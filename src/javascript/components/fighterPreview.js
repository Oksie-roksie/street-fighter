import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  
  if (fighter) {
    // const fighterName = createElement({ tagName: 'h4', className: 'fighter-preview___name' });
    // fighterName.innerText = fighter.name;
    const fighterImage = createFighterImage(fighter);
    const detailsContainer = createElement({tagName: 'div', className: 'fighter-preview___details'});
    detailsContainer.insertAdjacentHTML('beforeend', `
      <h4>${fighter.name}</h4>
      <p>Health: <span>${fighter.health}</span></p>
      <p>Attack: <span>${fighter.attack}</span></p>
      <p>Defense: <span>${fighter.defense}</span></p>
    `)

    fighterElement.append(fighterImage, detailsContainer);
  }

  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
