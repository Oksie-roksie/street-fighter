import { createElement } from '../../helpers/domHelper';
import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';
import App from '../../app';

export function showWinnerModal(fighter) {
 
  const title = `The winner is ${fighter.name}!`;
  const bodyElement = createWinnerElement(fighter);
  onClose = () => {
    const arena = document.querySelector('.arena___root');
    arena.remove();
    new App();
  }

  showModal({ title, bodyElement, onClose });

  // call showModal function
}

function createWinnerElement(fighter) {
  const fighterElement = createElement({
    tagName: 'div',
    className: 'winner',
  });
  if (fighter) {
    const fighterImage = createFighterImage(fighter);

    fighterElement.append(fighterImage);
  }
  return fighterElement;
}
