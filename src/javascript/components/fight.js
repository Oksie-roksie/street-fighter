import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

    if (firstFighter.health <= 0 || secondFighter.health <= 0) {
      const winner = firstFighter.health <= 0 ? secondFighter : firstFighter;
      resolve(winner);
      }

    const firstFighterHealthBar = document.getElementById('left-fighter-indicator');
    const secondFighterHealthBar = document.getElementById('right-fighter-indicator');

    window.addEventListener('keyup', function(evt) {
      if (evt.code === controls.PlayerOneAttack) {

        const damage = getDamage(firstFighter, secondFighter);
        secondFighter.health -= damage;
        const percent = secondFighter.health < 0 ? 0 : (secondFighter.health - damage) / secondFighter.health * 100;
        secondFighterHealthBar.style.width = `${percent}%`;
      }
      if (evt.code === controls.PlayerTwoAttack) {
         
        const damage = getDamage(secondFighter, firstFighter);
        firstFighter.health -= damage;
        const percent = firstFighter.health < 0 ? 0 : (firstFighter.health - damage) / firstFighter.health * 100;
        firstFighterHealthBar.style.width = `${percent}%`;
      }
      if (evt.code === controls.PlayerOneBlock) {
        getBlockPower(firstFighter)
      }
      if (evt.code === controls.PlayerTwoBlock) {
        getBlockPower(secondFighter)
      }
    })
  
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  let damage = hitPower - blockPower;
  damage = damage > 0 ? damage : 0;
  
  return damage
  // return damage
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const hitPower = fighter.attack * criticalHitChance;

  return hitPower
  // return hit power
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  const blockPower = fighter.defense * dodgeChance;

  return blockPower
  // return block power
}

function getCriticalHit(fighter) {
  const criticalHit = 2 * fighter.attack;

  return criticalHit
}
